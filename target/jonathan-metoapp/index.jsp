<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
    <body>
        <h2>What weather is it?</h2>

        <form id="adress-form" action="weather" method="GET">
            <label for="location">City</label> 
            <input type="text" name="location" id="location" autocomplete="on">
            <div>
                <input type="text" name="location-result" id="location-result">
            </div>
            <div>
                latitude: <input type="text" name="lat" id="lat">
                Longitude: <input type="text" name="lon" id="lon">
            </div>
        <button id="find-coordinated">OK</button>
        </form>
        
        <script src="index.js"></script>
    </body>
</html>