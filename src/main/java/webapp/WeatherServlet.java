package webapp;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

@WebServlet("/weather")
public class WeatherServlet extends HttpServlet {
    private static final String OpenWeatherMap_KEY = "87d477157c3d38384553027b38f5e618";
    private static final String OpenWeatherMap_URL = "https://api.openweathermap.org/data/2.5/weather?lat=%s&lon=%s&appid=%s";

    // pour que le nom de classe ne soit pas surligné, juste pour VScode
    private static final long serialVersionUID = 1L;

    private String getWeather(String lat, String lon) throws IOException, ParseException {
        
        HttpURLConnection connection = null;
        URL url = new URL(
            String.format(OpenWeatherMap_URL, lat, lon, OpenWeatherMap_KEY)
        );
        connection = (HttpURLConnection) url.openConnection();
        connection.addRequestProperty("User-Agent", "MeteoWeatheIsIt/Java 1.0");

        // Parse Request
        JSONParser parser = new JSONParser();
        JSONObject result = (JSONObject) parser.parse(
            new InputStreamReader(connection.getInputStream()));
        JSONArray weather = (JSONArray) result.get("weather");
        JSONObject weather_0 = (JSONObject) weather.get(0);
        String description = (String) weather_0.get("description");

        return description;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
        throws ServletException, IOException {
            String lat = req.getParameter("lat");
            String lon = req.getParameter("lon");
            
            String result;
            try {
                result = this.getWeather(lat, lon);
                resp.getWriter().println("Result: "+ result);

                req.setAttribute("result", result);
                req.getRequestDispatcher("response.jsp")
                    .forward(req, resp);
            } 
            catch (Exception e) {
                resp.getWriter().println(
                    "An error occured: " + e.getLocalizedMessage());
            }

            resp.getWriter().println("Find out the Weather");
            resp.getWriter().println("latitude: "+ lat);
            resp.getWriter().println("longitude: "+ lon);
            
    }
}