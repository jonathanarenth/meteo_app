document.addEventListener("DOMContentLoaded", function () {
    getGPSCoordianated()
})

function getGPSCoordianated(){
    
    document.querySelector('form')
        .addEventListener("submit", (e) => {
            e.preventDefault()
            var ressourceLocation = 
                document.querySelector("#location").value;
            var urlNominatim = 
                'https://nominatim.openstreetmap.org/search?q='+ 
                ressourceLocation + '&format=json&limit=1'
    
            fetch(urlNominatim)
                .then((response) => {
                    if (response.ok) {
                        return response.json()
                    } else {
                        // get error 404
                        window.alert(
                        "server response : " + response.status)
                    }
            })
            // the server didn't response
            .catch((error) => {
                window.alert("something went wrong : " + error)
            })
            .then((data) => {
                //console.log(data)
                const locationName = data[0].display_name
				const locationLong = data[0].lon
				const locationLat = data[0].lat

                document.getElementById("location-result").value = locationName
				document.getElementById("lon").value = locationLong
                document.getElementById("lat").value = locationLat
            }).then(()=>{
                document.querySelector('form').submit()
            })
    })
}
